console.log('Hello, World!');

//Activity 3-4

	function getCube(num1) {
		let cubeFrmla = num1 ** 3;
		console.log(`The cube of ${num1} is ${cubeFrmla}`);
	}
	getCube(3);



// Activity 5-6

	const address = ['Block 1 Lot 1', 'S. Perry Street', 'Melbourne','Australia'];

	const [addLine1, addLine2, addLine3, addLine4] = address;

	console.log(`I live at ${addLine1} ${addLine2} ${addLine3} ${addLine4}`);


// Activity 7-8

	const animal = {
		name: 'Lolong',
		species:'Salt Walter Crocodile',
		weight: '1075 kgs',
		measurement: '20 ft 3 in'
	};


	const {name, species, weight, measurement} = animal;

	function getFullAddress({name, species, weight, measurement}) {
		console.log(`${name} was a ${species}. He weighed ${weight} with a measurement of ${measurement}.`)
	}
	getFullAddress(animal);


// Activity 9-10
	
	const numbers = [1,2,3,4,5];
	numbers.forEach((number) => {
		console.log(number);
	})
	// numbers.forEach((number) => console.log(number))

// Activity 11
	let reduceNumber = numbers.reduce((x, y) => x + y);
	console.log(reduceNumber);	
	

// Activity 12-13
	class dog {
		constructor(name, age, breed) {
			this.name = name;
			this.age = age;
			this.breed = breed;
		}
	}

	const myDog = new dog('Frankie',5,'Miniature Dachsund');

	// myDog.name = 'Frankie';
	// myDog.age = 5;
	// myDog.breed = 'Miniature Dachsund';
	console.log(myDog);



